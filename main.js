
function getLocation(callback) {
  $("#progress").toggle();
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(callback);
  } else {
    $("#currentLocationGps").text("Geolocation not supported!")
  }
}

function showPosition(position) {
  console.log(position);
  $("#currentLocationGps").text(
      "Latitude: " + position.coords.latitude +
      ", Longitude: " + position.coords.longitude);
}

// TODO -- static data for now
// <wpt lat="51.315418" lon="9.500508">
//   <name><![CDATA[22kW Ladenetz in Kassel]]></name>
//   <desc><![CDATA[Störmelderufnummer: +49 561 5745-2244, Entenanger in 34117 Kassel - Entenanger 9, 1 x Typ 2 22 kW, 1 x Typ 2 11 kW, 2 x Schuko, andere, Parkplätze 2, ]]></desc>
// </wpt>
function findNearByChargingStations(position) {
  $("#stations").text("searching...");

  /*
  var data = [
    {
      @lat: 51.315418,
      @lon: 9.500508,
      name: "22kW Ladenetz in Kassel",
      desc: "Störmelderufnummer: +49 561 5745-2244, Entenanger in 34117 Kassel - Entenanger 9, 1 x Typ 2 22 kW, 1 x Typ 2 11 kW, 2 x Schuko, andere, Parkplätze 2, "
    },
    {
      @lat: 48.299545,
      @lon: 14.306788,
      name: "22kW Linz AG in Linz",
      desc: "Design Center 2 in 4020 Linz - Goethestraße 82, 2 x Typ 2 22 kW, 2 x Typ 2 3,7 kW, andere, Manchmal bei Veranstaltungen gesp    errt, Parkplätze 4, , Parkkosten €1,- / 30min^1 Dauerparkkarten erhältlich"
    },
  ];
  */

  var data = [];
  var stations = getStations();

  var maxDistance = getMaxDistance();
  var typeForFilter = getTypeForFilter();
  stations.forEach(function (station) {
    var distance = getDistanceFromLatLonInKm(station['@lat'], station['@lon'], position.coords.latitude, position.coords.longitude);
    if ((distance < maxDistance) &&
        ((typeForFilter === "") || (station['desc'].indexOf(typeForFilter) >= 0))) {
      station.distance = distance; // needed for sorting
      data.push(station);
    }
  });

  data.sort(sortByDistance);

  return data;
}

function getMaxDistance() {
  return $.jStorage.get("maxDistance", 50);
}

function getTypeForFilter() {
  return $.jStorage.get("typeForFilter", "");
}

// http://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value-in-javascript
function sortByDistance(a, b) {
  if (a.distance < b.distance) {
    return -1;
  }

  if (a.distance > b.distance) {
    return 1;
  }
  return 0;
}

function showPositionAndFindChargingStations(position) {
  showPosition(position);
  var data = findNearByChargingStations(position);
  console.log(data);
  showChargingStations(data, position);
  $("#progress").toggle();
}

function showChargingStations(data, position) {
  var s = "";

  var limitForGeLinks = 10;
  if (data.length > 0) {
    s = "<small>" + data.length +" records found.</small><br><br>";
    data.forEach(function (item) {
      s += "<b>" + item.name + "</b>"
      + "<br>"
      + "<small>"
      + " @ " + item['@lat'] + " / " + item['@lon']
      + ", distance: " + getDistanceFromLatLonInKm(item['@lat'], item['@lon'], position.coords.latitude, position.coords.longitude, 100) + " km"
      + "</small>"
      + " - " + generateMapsLink(item['@lat'], item['@lon'], position.coords.latitude, position.coords.longitude)
      + ((limitForGeLinks-- > 0) ? generateGeLink(item['@lat'], item['@lon']) : "")
      + "<br>"
      + " (" + item.desc + ")"
      + "<br>&nbsp;<br>";
    });
  } else {
    s = "(no result)";
  }

  $("#stations").html(s);
}

function generateGeLink(lat, lon) {
  var result = "";

  $.ajax({
    url: "/getGeLink.php",
    dataType: 'json',
    async: false,
    data: {
      lat: lat,
      lon: lon
    },
    success: function(data) {
      console.log("success");
      result =
          " <a href=\""
          + data['url']
          + "\" target=\"_blank\">GE</a>";
    },
    error: function(data) {
      console.log("error");
      result = " (GE failed)";
    }
  });

  // target="_blank"
  return result;
}

function loadJson() {
  console.log("loadJson");

  $("#stations").text("loading...");

  $.getJSON("GoingElectric.json", function(data) {
    console.log("GoingElectric.json loaded");
    $.jStorage.set("stations", data);
    $.jStorage.set("stations.loadTime", new Date());
    $("#loadTime").text($.jStorage.get("stations.loadTime")); // hack

    // dirty, should be done after above has been done
    $.getJSON("GoingElectric-meta.json", function(data) {
      console.log("GoingElectric-meta.json loaded", data);
      $.jStorage.set("stations.lastUpdate", data['exported-at']);
      $("#lastUpdate").text($.jStorage.get("stations.lastUpdate")); // hack
    });

    $("#stations").text("loaded.");
  });
}

// http://stackoverflow.com/questions/1801732/how-do-i-link-to-google-maps-with-a-particular-longitude-and-latitude
function generateMapsLink(lat1, lon1, lat2, lon2) {
  return ""
      + "<a href=\""
      // + "https://www.google.com/maps/@" + lat1 + "," + lon1 + ",12z\""
      // + "https://www.google.com/maps/place/" + lat1 + "," + lon1 + "\""
      + "https://www.google.com/maps/dir/'" + lat1 + ", " + lon1 + "'/'" + lat2 + "," + lon2 + "'\""
      + " target=\"_blank\">Map</a>"
}

// http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2, round) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
          Math.sin(dLat / 2) * Math.sin(dLat / 2) +
          Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
          Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  if (round > 0) {
    return Math.round(d * round) / round; // TODO refactor
  } else {
    return d;
  }
}

function deg2rad(deg) {
  return (deg * (Math.PI/180));
}

function getStations() {
  return ($.jStorage.get("stations") != null) ? $.jStorage.get("stations")['gpx']['wpt'] : { };
}

function reload() {
  console.log("forced reloading");

  $.jStorage.deleteKey("stations");
  $.jStorage.deleteKey("stations.loadTime");
  $.jStorage.deleteKey("stations.lastUpdate");

  loadJson();
}

function update() {
  var typeForFilter = $("#type").val();
  $.jStorage.set("typeForFilter", typeForFilter);

  var maxDistance = $("#maxDistance").val();
  $.jStorage.set("maxDistance", maxDistance);

  getLocation(showPositionAndFindChargingStations); // TODO I must vomit!
}

function init() {
  $("#progress").toggle(); // hack
  $("#type").val($.jStorage.get("typeForFilter", "")).change();
  $("#maxDistance").val($.jStorage.get("maxDistance", 50)).change();

  if ($.jStorage.get("stations", 0) === 0) {
    loadJson(); // TODO callback
  } else {
    console.log("Data already loaded.");
    $("#loadTime").text($.jStorage.get("stations.loadTime"));
    $("#lastUpdate").text($.jStorage.get("stations.lastUpdate"));
  }

  $("#forceReload").click(function (event) {
    event.preventDefault();
    reload();
  });

  $("#update").click(function (event) {
    event.preventDefault();
    update();
  });

  getLocation(showPositionAndFindChargingStations);
}

// ---

$(document).ready(
  function () {
    init();
  });
