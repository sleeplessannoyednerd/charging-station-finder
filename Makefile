.PHONY:

include credentials.mk

GoingElectric.json: GoingElectric.gpx
	/Users/dietmar/Applications/xml2json/xml2json.py -t xml2json -o GoingElectric.json GoingElectric.gpx
	sed -i.bak -e 's#{http://www.topografix.com/GPX/1/1}##g' GoingElectric.json
	rm GoingElectric.json.bak

	echo "{\"exported-at\": \"`date +%Y-%m-%d`\"}" > GoingElectric-meta.json

update: download GoingElectric.json

download:
	curl -s -o /dev/null -c /tmp/GoingElectric.cookies 'http://www.goingelectric.de/forum/ucp.php?mode=login' -H 'Origin: http://www.goingelectric.de' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en,en-US;q=0.8,de-DE;q=0.6,de;q=0.4' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: http://www.goingelectric.de/stromtankstellen/poi/' -H 'Connection: keep-alive' --data "username=${USER}&password=${PASSWORD}&login=Anmelden&redirect=http%3A%2F%2Fwww.goingelectric.de%2Fstromtankstellen%2Fpoi%2F" --compressed
#	curl -b /tmp/GoingElectric.cookies 'http://www.goingelectric.de/forum/' -H 'Origin: http://www.goingelectric.de' -H 'Accept-Language: en,en-US;q=0.8,de-DE;q=0.6,de;q=0.4' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: http://www.goingelectric.de/stromtankstellen/poi/' -H 'Connection: keep-alive'
	curl -s -b /tmp/GoingElectric.cookies -o GoingElectric.gpx -X POST 'http://www.goingelectric.de/stromtankstellen/poi/poi-download2.php' -H 'Origin: http://www.goingelectric.de' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: http://www.goingelectric.de/stromtankstellen/poi/' -H 'Connection: keep-alive' --data 'form_typ=gpx&form_laender%5B%5D=Oesterreich&form_laender%5B%5D=Deutschland' --compressed

