<?php

function extractUrl($data) {
  $content = $data['content'];
  $content = str_replace("\\", "", $content); # needed?

  $start = strpos($content, "http://");
  $end = strpos($content, "\">Details</a>");

  return substr($content, $start, ($end - $start));
}

function extractId($data) {
  return $data[0]->id;
}

$lat = $_GET['lat'];
$lon = $_GET['lon'];

$s = curl_init(); 

curl_setopt($s, CURLOPT_URL, "http://www.goingelectric.de/stromtankstellen/");
#curl_setopt($s,CURLOPT_HTTPHEADER,array('Expect:'));
#curl_setopt($s,CURLOPT_TIMEOUT,$this->_timeout);
#curl_setopt($s,CURLOPT_MAXREDIRS,$this->_maxRedirects);
curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
curl_setopt($s, CURLOPT_FOLLOWLOCATION, true);
#curl_setopt($s,CURLOPT_COOKIEJAR,$this->_cookieFileLocation);
#curl_setopt($s,CURLOPT_COOKIEFILE,$this->_cookieFileLocation);

$result = curl_exec($s);

# print_r($result);

// ---

// barrierefrei=false&nwlng=15.450072&hotel=false&swlat=46.916230999999996&notverifiziert=false&kostenlos=false&nwlat=48.116231&kostenlosparken=false&keinestoerung=true&verbund%5B%5D=alle&ladekarte%5B%5D=alle&oeffnungszeiten=false&stecker%5B%5D=alle&fotos=false&typ=SucheZoom&swlng=13.650072&stoerung=false&restaurant=false&verifiziert=false&

$fields = array(
  barrierefrei => false,
  nwlng => $lon + 0.000001,
  hotel => false,
  swlat => $lat - 0.000001,
  notverifiziert => false,
  kostenlos => false,
  nwlat => $lat + 0.000001,
  kostenlosparken => false,
  keinestoerung => true,
  'verbund[]' => alle,
  'ladekarte[]' => alle,
  oeffnungszeiten => false,
  'stecker[]' => alle,
  fotos => false,
  typ => SucheZoom,
  swlng => $lon - 0.000001,
  stoerung => false,
  restaurant => false,
  verifiziert => false
);

$s = curl_init();
curl_setopt($s,CURLOPT_URL, "http://www.goingelectric.de/stromtankstellen/karte.php");
curl_setopt($s,CURLOPT_RETURNTRANSFER, true);
curl_setopt($s,CURLOPT_POST, true);
curl_setopt($s,CURLOPT_POSTFIELDS, http_build_query($fields));

$result = curl_exec($s);

#print_r($result);

# ---

$fields = array(
  id => extractId(json_decode($result))
);

$s = curl_init();
curl_setopt($s,CURLOPT_URL, "http://www.goingelectric.de/stromtankstellen/infowindow2.php");
curl_setopt($s,CURLOPT_RETURNTRANSFER, true);
curl_setopt($s,CURLOPT_POST, true);
curl_setopt($s,CURLOPT_POSTFIELDS, http_build_query($fields));

$result = curl_exec($s);

#print_r($result);

$data = json_decode($result, true);

# $data = json_decode('{"status":1,"content":"<div class=\"infowindow_main\"><div class=\"infowindow_name\">Trieben- Hochlandrinderzucht<\/div><div class=\"infowindow_address\">Sonnberg, Trieben<\/div><div class=\"infowindow_plugs\">1 x CEE Rot 11 kW<br>1 x Schuko<br><\/div><a class=\"infowindow_details\" target=\"_blank\" href=\"http:\/\/www.goingelectric.de\/stromtankstellen\/Oesterreich\/Trieben\/Trieben-Hochlandrinderzucht-Sonnberg-41\/3266\/\">Details<\/a>"}', true);

$result = array(
  url => extractUrl($data)
);

echo(json_encode($result));

